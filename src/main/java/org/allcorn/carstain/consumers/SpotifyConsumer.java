/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.consumers;

import org.allcorn.carstain.messages.CarMessage;
import org.allcorn.carstain.utils.Config;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * Created by allcos01 on 23/07/16.
 */
public class SpotifyConsumer extends BaseConsumer {

    boolean isPlaying = false;

    String spotifyCommandFifo;
    String spotifyResponseFifo;

    Logger log = Logger.getLogger(this.getClass());
    int track = 0;
    int playlist = 0;
    int playlistLength = 0;
    boolean startAtEnd = true;

    static final String cmdStart = new String("start_playlist");
    static final String cmdPause = new String("pause");
    static final String cmdPlaylistLength = new String("playlist_length ");

    public SpotifyConsumer() {

        spotifyCommandFifo = Config.getConfig().getSpotifyFifo();
        spotifyResponseFifo = Config.getConfig().getSpotifyFifo() + ".response";

        log.info("SpotifyConsumer consumer created");
    }

    public void consumeMessage(CarMessage message) {
        log.info("SpotifyConsumer consumer got message [" + message.getMessage().toString() + "]");

        if(message.getMessage() == CarMessage.id.STEERING_WHEEL_RECIRC) {
            if(!isPlaying) {
                startPlayback();
            }
            else {
                pausePlayback();
            }
        }
        if(message.getMessage() == CarMessage.id.STEERING_WHEEL_UP_TRACK) { nextTrack(); };
        if(message.getMessage() == CarMessage.id.STEERING_WHEEL_DOWN_TRACK) { prevTrack(); };

        //55 playlist is 460 tracks long, not seeming to work atm
        //50 is nats
        //49 is jamies
        //46 - Carter offline
        //53 / Abba offline
        //if(message.getMessage() == CarMessage.id.KEY_0_INSERTED) { selectPlaylist(55); };
        if(message.getMessage() == CarMessage.id.KEY_0_INSERTED) { selectPlaylist(55    ); };
        if(message.getMessage() == CarMessage.id.KEY_1_INSERTED) { selectPlaylist(53); };

        if(message.getMessage() == CarMessage.id.KEY_0_REMOVED) { pausePlayback(); };
        if(message.getMessage() == CarMessage.id.KEY_1_REMOVED) { pausePlayback(); };
        if(message.getMessage() == CarMessage.id.KEY_2_REMOVED) { pausePlayback(); };
        if(message.getMessage() == CarMessage.id.KEY_3_REMOVED) { pausePlayback(); };

    }

    private void startPlayback() {
        issueCommand(cmdStart + " " + playlist + " " + track);
        isPlaying = true;
    }

    private void nextTrack() {
        if (startAtEnd) { track--;} else { track++; }
        if(track < 0) { track = (playlistLength-1); }
        if(track >= playlistLength) { track = 0; }
        log.info("About to skip to next track [" + track + "]");
        startPlayback();
    }

    private void prevTrack() {
        if (startAtEnd) { track++;} else { track--; }
        if(track < 0) { track = (playlistLength-1); }
        if(track >= playlistLength) { track = 0; }
        log.info("About to skip to previous track [" + track + "]");
        startPlayback();
    }

    private void selectPlaylist(int playlist) {
        this.playlist = playlist;
        this.playlistLength = Integer.parseInt(issueCommandWithResult(cmdPlaylistLength + playlist));
        log.debug("Playlist [" + playlist + "] length = [" + playlistLength + "]");
        if(startAtEnd) { track = (playlistLength-1); } else { track = 0; }
        log.info("Changed playlist to [" + this.playlist + "]");
    }

    private void pausePlayback() {
        issueCommand(cmdPause);
        isPlaying = false;
    }

    private void issueCommand(String command) {

        log.debug("About to issue command [" + command +"]");
        try {
            BufferedWriter spotifyFifoWriter =
                    new BufferedWriter(new FileWriter(spotifyCommandFifo));
            spotifyFifoWriter.write(command + "\n");
            spotifyFifoWriter.flush();
            spotifyFifoWriter.close();
        } catch (IOException e) {
            log.error(e.toString());
            e.printStackTrace();
        }
    }

    private String issueCommandWithResult(String command) {

        issueCommand(command);
        String retVal = "";

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            log.error(e.toString());
        }

        try {

            BufferedReader spotifyFifoReader =
                    new BufferedReader(new FileReader(spotifyResponseFifo));;
            while(spotifyFifoReader.ready()) {
                log.debug("about to read...");
                retVal = spotifyFifoReader.readLine();
                log.debug("Just read String [" + retVal + "]");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.toString());
        }

        return  retVal;
    }

}
