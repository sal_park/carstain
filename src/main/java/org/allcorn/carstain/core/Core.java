/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.core;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.allcorn.carstain.consumers.SpotifyConsumer;
import org.allcorn.carstain.messages.CarMessage;
import org.allcorn.carstain.read.CarReader;
import org.allcorn.carstain.read.CarResponder;
import org.allcorn.carstain.utils.Config;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;

/**
 * This connects input and output processes via their associated pipes and 
 * calculates what the output should be based on input and also where it should be delivered to. An Example would be:<BR>
 * 1) Input process Canbus reader passes "UP" to core<BR>
 * 2) Core decides that this should be translated to "NEXT_TRACK"<BR>
 * 3) Core passes "NEXT_TRACK" to output process SpotifyConsumer.<BR>
 *
 * @author carstain@allcorn.org
 *
 */
public class Core implements CarResponder {
	
  Logger log = Logger.getLogger(this.getClass());
  String fifo;
  Document controlXML;

  SpotifyConsumer sp = new SpotifyConsumer();

  /**
   *  Constructor for the core object
   */
  public Core(String fifo, Document controlXML) {

      this.fifo = fifo;
      this.controlXML = controlXML;
	}
  
  /**
   * The main loop for the core package
   * Read all inputs, process and write 
   * appropriate output
   * 
   * @throws InterruptedException
   */
	public void runCore() throws InterruptedException,ParserConfigurationException, SAXException, IOException  {

        log.debug("About to start canbusReader...");
        CarReader canbusReader = new CarReader(fifo,controlXML,this);
        Thread t = new Thread(canbusReader);
        t.start();



        log.info("Waiting for events...");
	}

    // make sure this tread does not die so the messageHandler
    // is always available and we also want it running with all other
    // threads not running as daemon threads so that systemd can kill all
    // the processes easily.
    public  void waitForShutdown() {
        while(true) {
            waitForShutdown(1000);
        }
    }

    // needed for testing
    public  void waitForShutdown(int timeInMillis) {
        try {
            Thread.sleep(timeInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
	
	/**
	 *  One of the readers calls this method to pass
   *  back an action it has received on it's pipe
	 */
	public void messageHandler(CarMessage message) {
		log.debug("action received at [" + Long.toString(message.getTimeStamp()) + "ms]:" +
                message.getMessage().toString());

        sp.consumeMessage(message);

        log.trace("action process complete at [" + Long.toString(message.getTimeStamp()) + "ms]:" +
                message.getMessage().toString());
		
	}

}
