/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.core;

import org.allcorn.carstain.utils.Config;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Wrapper for the core object.
 * 
 * @author carstain@allcorn.org
 *
 */
public class CoreStartup {

  /**
   * Construct and start the Core object
   * 
   * @param args
   * @throws InterruptedException
   */
	public static void main(String[] args) throws InterruptedException,ParserConfigurationException,
            SAXException,IOException {

      Config systemConfig = Config.getConfig();

      Core cartCore = new Core(
              systemConfig.getCanbusControlFIFO(),
              systemConfig.getCarControlXML());
      
      cartCore.runCore();
      cartCore.waitForShutdown();
      
      
	}

}
