/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.messages;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;


/**
 * A lightweight object used to represent a single message passed from a CarReader.
 * 
 * @author carstain@allcorn.org
 *
 */
public class CarMessage {
	  
  public static enum id {
    STEERING_WHEEL_UP_TRACK,
    STEERING_WHEEL_DOWN_TRACK,
    STEERING_WHEEL_RECIRC,
    KEY_0_INSERTED,
    KEY_1_INSERTED,
    KEY_2_INSERTED,
    KEY_3_INSERTED,
	KEY_0_REMOVED,
	KEY_1_REMOVED,
	KEY_2_REMOVED,
	KEY_3_REMOVED,
	CAR_LOCK,
    CAR_UNLOCK,
	PASSENGER_DOOR_CLOSED,
	PASSENGER_DOOR_OPEN,
	DRIVER_DOOR_CLOSED,
	DRIVER_DOOR_OPEN};

  private static RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
	
	private id theMessage;
  private long timestamp;

  /**
   * Create the new message object based on the input string
   * 
   * @param theStringMessage
   * @throws RuntimeException thrown if theStringMessage is not known
   */
	public CarMessage(String theStringMessage) throws RuntimeException {
	    	  
	  try {
	    theMessage = id.valueOf(theStringMessage);
	  }
	  catch( java.lang.IllegalArgumentException e) {
      throw new RuntimeException("Unknown raw message [" + theStringMessage + "]");
    }
    	  
	  // record the date/time 
	  timestamp = rb.getUptime();
	  
	}
	
	/**
	 * 
	 * @return the enum representation of the message
	 */
	public id getMessage() {
	  return theMessage;
	}
	
	/**
	 * 
	 * @return the time in ms that this message was
   * created (relative to when the jvm was 
   * started)
	 */
	public long getTimeStamp() {
	  return timestamp;
	}

}
