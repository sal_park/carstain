/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.read;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Pattern;

import org.allcorn.carstain.core.Core;
import org.allcorn.carstain.messages.CarMessage;
import org.allcorn.carstain.utils.CarCommandQueue;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

/**
 * A class to connect to a named pipe and read the data. 
 * Data is expected to be ASCII text and contain exactly one command per line. 
 * This class will read in the commands and store them so that it can work out
 * what the output message should be.
 * 
 * @author carstain@allcorn.org
 *
 */
public class CarReader implements Runnable {
  Core coreRef;
  Document carControlXML;
  Logger log = Logger.getLogger(this.getClass());
  InputStreamReader pipeReader;
  private static final char newLine = '\n';
  StringBuilder nextCommand;
  char nextChar;

  private static RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();

  /*this will store all the car commands, messages and regexp to
   *look them up. Format is:
   *
   * busID -> regexp -> messageName
   */
  HashMap<String, HashMap<Pattern, String>> commandToMessageRegExp =
      new HashMap<String, HashMap<Pattern, String>>();

  // This will store the queues, 1 for each busID
  HashMap<String, CarCommandQueue> commandQueues =
      new HashMap<String, CarCommandQueue>();

      int busIdentifierLength = 0;

/**
 * Create a new CarReader object to read FIFO
 *
 * @param myPipeFile the filename of the named pipe
 * @param coreRef the object that has implemented org.allcorn.carstian.read.CarResponder
 */
public CarReader(String myPipeFile, Document carControlXML, Core coreRef) {
  this.coreRef = coreRef;
  this.carControlXML = carControlXML;
  try {
    File file = new File(myPipeFile);
    pipeReader = new InputStreamReader(new FileInputStream(file));
  } catch (FileNotFoundException e) {
    log.error(e.toString());
    e.printStackTrace();
  }
  nextCommand = new StringBuilder();
  log.debug("CarReader constructor complete");
}

/**
 *  Start this thread. Will loop forever (or until pipe is closed) reading
 *  the myPipeFile and then building/creating a CarMessages object to be
 *  passed back to the calling object
 *
 */
public void run() {

  // populate the hashmap commandToMessageRegExp which can
  // translate from raw commands to messages.
  initCarControlXML();

  while (true) {
    processCommand(getRawCommand());
  }
}

private void processCommand(String rawCommand) {
  String bus;
  String command;
  CarCommandQueue thisQueue;
  Set<Pattern> patterns;

  log.trace("Processing raw canbus cmd = [" + rawCommand + "]");

  if(rawCommand.length() > busIdentifierLength) {
    bus = rawCommand.substring(0,busIdentifierLength);
    command = rawCommand.substring(busIdentifierLength);

    if(commandQueues.containsKey(bus)) {

      // Get hold of the correct queue for this bus
      thisQueue = commandQueues.get(bus);

      log.trace("Adding command [" + command + "] to bus [" + bus + "] at [" + getUptime() + "ms]");
      thisQueue.add(command);

      log.trace("queue is [" + thisQueue.toString() + "]");

      // check to see if any pattern matches
      patterns = commandToMessageRegExp.get(bus).keySet();
      for (Pattern thisPattern : patterns) {
        log.trace("Checking pattern [" + thisPattern.pattern() + "]");
        if(thisQueue.match(thisPattern)) {
          String result = commandToMessageRegExp.get(bus).get(thisPattern);
          log.debug("Matched it to [" + result + "] at [" + getUptime() + "ms]");
          coreRef.messageHandler(new CarMessage(result));
        }
      }
    }
    else {
      log.trace("bus [" + bus + "] not found in config");
    }
  }
  else  {
    log.trace("rawCommand [" + rawCommand + "] was too short at  [" + rawCommand.length() + "] charaters long");
  }
}

private String getRawCommand() {

  nextCommand.setLength(0);

  try {
    while( (nextChar = (char) pipeReader.read()) != -1 ) {
      if( nextChar == newLine) {
        break;
      }
      nextCommand.append(nextChar);
    }
  } catch (IOException e) {
    log.error(e.toString());
    e.printStackTrace();
  }

  return nextCommand.toString();
}

  String getUptime() {
    return Long.toString(rb.getUptime());
  }

/**
 *   Read the car control XML and parse in object to decode
 *   the commands from the car into messages.
 */
private void initCarControlXML() {

  carControlXML.getDocumentElement().normalize();

  //TODO validate the xml against the xsd
    
  busIdentifierLength = Integer.parseInt(carControlXML.
          getElementsByTagName("busIdentifierLength").item(0).getTextContent());

  NodeList busList = carControlXML.getElementsByTagName("bus");
  log.debug("Bus ID length[" + busIdentifierLength + "]");
  log.debug("Number of buses defined = [" + busList.getLength() + "]");
  for(int thisBus = 0; thisBus < busList.getLength(); thisBus++) {
    Element busElement = (Element) busList.item(thisBus);

    String busID = busElement.getAttribute("ID");

    int busQueueSize = Integer.valueOf(busElement.getAttribute("queueSize"));
    int busCommandSize = Integer.valueOf(busElement.getAttribute("commandSize"));

    log.debug("Bus [" + thisBus +"] has ID[" + busID + "] QueueSize[" +
      busQueueSize + "] CommandSize[" + busCommandSize + "]");

    NodeList messageList = busElement.getElementsByTagName("message");

    HashMap<Pattern, String> messages = new HashMap<Pattern, String>();

    for(int thisMessage = 0; thisMessage < messageList.getLength(); thisMessage++) {
      Element messageElement = (Element) messageList.item(thisMessage);

      String messageName = messageElement.getAttribute("name");
      String messageRegExp =
        messageElement.getElementsByTagName("commandRegexp").
        item(0).getTextContent();
      log.debug("  busID[" + busID + "] message name[" + messageName +"] regexp[" +
        messageRegExp + "]");
      Pattern pattern = Pattern.compile(messageRegExp);
      messages.put(pattern, messageName);
    }

    // add all the messages/regexps to the bus
    commandToMessageRegExp.put(busID, messages);

    //Create a queue object for this bus
    commandQueues.put(busID, new CarCommandQueue(busQueueSize,busCommandSize));

    }
  }
}
