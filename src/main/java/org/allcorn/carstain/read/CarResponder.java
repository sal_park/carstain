/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.read;

import org.allcorn.carstain.messages.CarMessage;

/**
 * An interface that CarReader.java will use to notify interested parties that an event has occurred.
 * 
 * @author carstain@allcorn.org
 *
 */
public interface CarResponder {
	
  /**
   * This function is called when a message object is ready
   * to be sent back to the class that implemented this
   * interface.
   * 
   * @param message the object representing the message
   */
	public void messageHandler(CarMessage message);

}
