/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.utils;

import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An singleton object to store all the current commands before they are processed.
 * This is important as some messages (e.g. fast forward in current track)
 * need to be able to see some history of previous commands before it can work,
 * so for example if you could only see a single command that the 'up' arrow had 
 * been pressed, it's not possible to tell if this is intended to change to the next
 * track or if it's the start of a fast forward skip through the current track. 
 * This list is ordered and filled from the top, read from the bottom. Once it reaches
 * the maximum size, the oldest command will be removed when a new command is added
 *
 * @author carstain@allcorn.org
 *
 */
public class CarCommandQueue {
  
  // Do not let list grow above this size
  private int commandLengthInChars = 0;
  private int maxListLenghtInChars = 0;
  
  //A string representation of the list. Updated when a new entry is added
  //This is used for the regex comparision.
  private String listAsString;
   
  //private static LinkedList<String> carList = new LinkedList<String>();
  private StringBuilder carList = new StringBuilder();

  private Logger log = Logger.getLogger(this.getClass());

  /**
   * Set the size of the queue.
   * @param maxCommandCount the maximum number of command the list will store
   * @param commandLengthInChars the number of characters in each command (nb):
   *                             The length must always be the same for every
   *                             command
   * @throws exception if called more than once
   */
  public CarCommandQueue(int maxCommandCount, int commandLengthInChars) {
    this.commandLengthInChars = commandLengthInChars;   
    //This how big we will let the list to grow to
    this.maxListLenghtInChars = maxCommandCount * commandLengthInChars;
         
  }
  
  /**
   * Add a new command onto the top of the list. 
   * If the size of the list exceeds maxListSize then
   * remove the oldest command on the list.
   * <BR>New commands are append to the right of the list.
   * The oldest command is deleted from the left of the list 
   *
   * @param the command to add
   */
  public void add(String command) {
    
    carList.append(command);
    
    if(carList.length() > maxListLenghtInChars) {
      // Removing oldest entry to stop carList
      // going over the maximum size
      carList.delete(0,commandLengthInChars);
    }
    
    //Update the string for the regexp
    listAsString = carList.toString();

  }
  
 
  
  /**
   * Get the number of commands currently in the list
   * @return the number of commands stored
   */
  public int size() {
   
    return (carList.length() / commandLengthInChars);
  }
  
  /**
   *  Number of chars that the list is using
   *  @return the number of chars used
   */
  public int capacity() {
    
    return carList.capacity();
  }
  
  
  /** Check to see if the messageToTest matches the head of the list.
   * Every item in the array messsageToTest much equal each element
   * it the carList collection. It is important to note that the match will
   * only be done once starting the comparison from the oldest element
   * in the carList object. It is expected this method will be called 
   * one or more times until a match is made. When a match is made,
   * all matching elements in the carList collection will be removed.
   * <BR>
   * -------------------------------------------------------------------<BR>
   * This is an example of a true result:<BR>
   * <BR>
   * carList         messageToTest<BR>
   * 12              12<BR>
   * 14              14<BR>
   * 16              16<BR>
   * 12<BR>
   * 12<BR>          
   * 13<BR>               
   * 15<BR>              
   * <BR>
   * the above true result will leave the carList like this:<BR>
   * <BR>
   * carList<BR>
   * 12<BR>
   * 12<BR>
   * 13<BR>
   * 15<BR>
   * <BR>
   * ----------------------------------------------------------------<BR> 
   * This is an example of a false result:<BR>
   * <BR>
   * carList         messageToTest<BR>
   * 12              14<BR>
   * 14              16<BR>
   * 16              12<BR>
   * 12<BR>
   * 12<BR>          
   * 13<BR>               
   * 15<BR>              
   * <BR>
   * the above false result will not change the contents of the carList<BR>
   * <BR>
   * @pattern the regexp pattern to test 
   * @return true when a match is found, false otherwise. All matched 
   * commands will be removed from the queue.
   */
   public boolean match(Pattern pattern) {
    boolean result = false;
    
    Matcher matcher = 
        pattern.matcher(listAsString);
    
    if(matcher.find()) {
        //found the first match
        log.trace("Match start=[" + matcher.start() +"] end=[" + matcher.end() +"]");
        log.trace("Matcher = [" + matcher.toString() +"]");

        result = true;

        // remove the commands that we've matched from the list
        carList.delete(0, matcher.end());

        //Update the string for the regexp
        listAsString = carList.toString();
    }
    
    if((!result) && (carList.length() > maxListLenghtInChars)) {
      //No match - remove the oldest command from the queue
      carList.delete(0,commandLengthInChars);
    }
    
    return result;
  }
  
  
  /**
   * only used for junit testing
   * 
   *  Display current contents of list for debug.
   * 
   *  @heading print this before each item in the carList.
  */
  public void printList(String heading) {
    
    System.out.println(heading + ": [" + carList.toString() + "]");
    
  }
  
  public String toString() {
    return carList.toString();
  }

}
