/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class is a singleton to represent the configuration data. This may 
 * come from environment variables, java properties files or xml files. 
 * 
 * @author carstain@allcorn.org
 *
 */
public class Config {
  
  private static Config myConfig = null;
  private static Map<String, String> env;
  private static String canbusControlFIFO;
  private static Logger log;
  
  private Config() {
    log = Logger.getLogger(this.getClass());
    env = System.getenv();
  }
  
  /**
   * Create and populate config object.
   * 
   * @return the singleton config object
   */
  public static Config getConfig() {
    
    if(myConfig == null) {
      myConfig = new Config();
    }
  
    return myConfig;
    
  }

  /**
   *  Read the environment variable and return it
   * 
   * @throws RuntimeException if the environment var does not exist
   */
  private static String getEnvVar(String envVarName) throws RuntimeException {
    
    if(env.containsKey(envVarName)) {
      log.debug("EnvVar [" + envVarName + "] value[" + env.get(envVarName) + "]");
      return (String) env.get(envVarName);
    }
    else {
      throw new RuntimeException("Missing environment variable [" + envVarName + "]");
    }
      
  }

  /**
   * Get the filename for the canbus FIFO
   * 
   * @return the canbus FIFO file
   */
  public String getCanbusControlFIFO() {
    return this.getEnvVar("CANBUS_CONTROL_FIFO");
  }

  public String getCarstainHome() { return this.getEnvVar("CARSTAIN_HOME"); }

  public String getSpotifyFifo() { return this.getEnvVar("SPOTIFY_CONTROL_FIFO"); }




  /**
   * Get the xml object that can read the data commands from the car
   * and convert them to carstain messages
   * 
   * @return the xml document dom object
   * @throws ParserConfigurationException 
   * @throws IOException 
   * @throws SAXException 
   */
  public Document getCarControlXML() throws ParserConfigurationException, SAXException, IOException {
    
    //TODO - the xml file name needs to come from carstain.properties
    String controlResource = "ControlImplementations/bmw-steering.xml";

    log.debug("Return carControlXML from [" + controlResource + "]");

    //String controlResource = "/Users/allcos01/carstain/carstain/src/main/resources/ControlImplementations/bmw-steering.xml";
    InputStream controlStream = ClassLoader.getSystemResourceAsStream(controlResource);
    
    if(controlStream != null) {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      return dBuilder.parse(controlStream);
    }
    else {
      throw new RuntimeException("Could not find car control resource [" + controlResource + "]");
    } 
  }
  
  

}
