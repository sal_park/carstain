package Elcanbus::CanBus;

##############################################################################
#                                                                            #
#  ELcanbus - a tool to aid canbus investigation and reverse engineering.    #
#                                                                            # 
#  Copyright (C) 2014 Simon Allcorn                                          # 
#                                                                            #
#  This file is part of Elcanbus.                                            #
#                                                                            #
#  Elcanbus is free software: you can redistribute it and/or modify          #
#  it under the terms of the GNU General Public License as published by      # 
#  the Free Software Foundation, either version 3 of the License, or         #
#  (at your option) any later version.                                       #
#                                                                            #
#  Elcanbus is distributed in the hope that it will be useful,               #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU General Public License for more details.                              #
#                                                                            #
#  You should have received a copy of the GNU General Public License         #
#  along with ELcanbus.  If not, see <http:#www.gnu.org/licenses/>.          #
#                                                                            #
##############################################################################

use Device::SerialPort;
use Time::HiRes qw (usleep);
use Term::ReadKey;
use Switch;

use strict;
use warnings;

use Exporter qw(import);
 
our @EXPORT_OK = qw(setupELM327SerialPort runCommand runCommandWithPause 
  runCommandAndGetResult runCommandExpectResult runCommandExpectOK 
  readRollingSummaryFromBus readFullDetailFromBus readFromBusNoOutput
  runCommandReturnSucess setupELM327SerialPortWithSpeed runCommandExpectPrompt 
  setPPvalue );

my $false = 0;
my $true = 1;


# setup the serial port
# to connect to the ELM327 device
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @portName - the device to connect to
#             e.g. /dev/ttyUSB0
#
# @portSpeed - the speed to connect at
#
# @return the port object
sub setupELM327SerialPortWithSpeed {

  my $debug = $_[0];
  my $portName = $_[1];
  my $portSpeed = $_[2];

  if ( $debug ) { print "Configure serial...\n"; }

  my $port = new Device::SerialPort ($portName, 1) || 
    die "Can't open Serial Port [".$portName."]: $!\n";

  $port->baudrate ($portSpeed);
  $port->parity ('none');
  $port->databits (8);
  $port->stopbits (1);

  $port->handshake ('none');

  $port->stty_icrnl (1);

  $port->stty_onlcr (1);
  $port->stty_opost (1);
  
  $port->error_msg(1);
  $port->user_msg(1);

  $port->write_settings ();

  if ( $debug ) { print "baud from configuration: [".$port->baudrate."]\n"; }

  return $port;
}

# setup the serial port
# to connect to the ELM327 device
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @portName - the device to connect to
#             e.g. /dev/ttyUSB0
#
# @return the port object
sub setupELM327SerialPort {

  my $debug = $_[0];
  my $portName = $_[1];

  return setupELM327SerialPortWithSpeed ( $debug, $portName, 115200 );
}

# set a program parameter in the ELM327
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
# @index    - the parameter index
#
# @value    - the value to store
#
sub setPPvalue {

  my $debug = $_[0];
  my $port  = $_[1];
  my $index = $_[2];
  my $value = $_[3];

  runCommandExpectOK($debug,$port,"AT PP ".$index." SV ".$value);
  runCommandExpectOK($debug,$port,"AT PP ".$index." ON");
}

# run a command on the ELM327
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
# @command  - the command to run
#
sub runCommand{

  my $debug = $_[0];
  my $port = $_[1];
  my $command = $_[2];
  
  if($debug) { print "Running command[".$command."]\n"; }

  $port->write($command."\r");
}

# run a command on the ELM327
# and give it some time to run
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
# @command  - the command to run
#
sub runCommandWithPause{

  my $debug = $_[0];
  my $port = $_[1];
  my $command = $_[2];

  runCommand($debug,$port,$command);

  usleep (1000000);
}

# run a command on the ELM327
# and give it some time to run
# and get the result
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
# @command  - the command to run
#
# @return the command response
#
sub runCommandAndGetResult{

  my $debug = $_[0];
  my $port = $_[1];
  my $command = $_[2];

  runCommandWithPause($debug,$port,$command);

  #my $result = $port->lookfor(500); 
  my $countIn;
  my $charRead;
  my $result = "";

  do {
    ($countIn, $charRead) = $port->read(255); 
    $result = $result.$charRead;
  } until ($countIn < 1);

  if($debug) { print "Result:\n--------------------------------\n".
    $result."\n--------------------------------\n"; }
  
  return $result;
}

# run a command on the ELM327
# and check that the result contains
# some defined text. Return true if this
# has worked, false otherwise
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
# @command  - the command to run
#
# @result   - the text to check in
#             the result
#
# @return   - boolean true on sucess,
#             false otherwise
#
sub runCommandReturnSucess{

  my $debug = $_[0];
  my $port = $_[1];
  my $command = $_[2];
  my $result = $_[3];

  my $output = runCommandAndGetResult($debug,$port,$command);

  if($output !~ /$result/s ) {
    return $false;
  }
  else {
    return $true;
  }
}

# run a command on the ELM327
# and give it some time to run
# and check that the result contains
# some defined text
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
# @command  - the command to run
#
# @result   - the text to check in
#             the result
#
sub runCommandExpectResult{

  my $debug = $_[0];
  my $port = $_[1];
  my $command = $_[2];
  my $result = $_[3];

  if(!runCommandReturnSucess($debug,$port,$command,$result)) {
    die "Command [$command] gave unexpected result, expecting [$result]";
  }
}

# run a command on the ELM327
# and give it some time to run
# and check that the result contains
# 'OK'
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
# @command  - the command to run
#
sub runCommandExpectOK{

  my $debug = $_[0];
  my $port = $_[1];
  my $command = $_[2];
  runCommandExpectResult($debug,$port,$command,"OK");
}

# Get a prompt from the ELM327
# to know it's ready because we
# see a '>'
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @port     - the port object
#
sub runCommandExpectPrompt{

  my $debug = $_[0];
  my $port = $_[1];
  runCommandExpectResult($debug,$port,"","[>]*");
}

# Read data from the canbus and display a
# summary
#
# @debug        - 1 to turn debug on
# (mandatory)
#
# @portName     - the device to connect to
# (mandatory)     e.g. /dev/ttyUSB0
#
# @return hash of {canID}{canData}
#
sub readRollingSummaryFromBus {

  my $debug       = $_[0];
  my $port        = $_[1];

  return readDataFromBus($debug,$port,"RollingSummary"); 
}

# Read data from the canbus and display 
# Full detail for CanBus ID's that match the
# regular expression
#
# @debug        - 1 to turn debug on
# (mandatory)
#
# @portName     - the device to connect to
# (mandatory)     e.g. /dev/ttyUSB0
#
# @canIDRegexp  - a regexp used to restrict
# (mandatory)     what is read from the canbus
#                 to STDOUT
#
# @return hash of {canID}{canData}
#
sub readFullDetailFromBus {

  my $debug       = $_[0];
  my $port        = $_[1];
  my $canIDRegexp = $_[2];

  return readDataFromBus($debug,$port,"FullDetail",$canIDRegexp); 
}

# Read data from the canbus but don't display
# any details to STDOUT
#
# @debug        - 1 to turn debug on
# (mandatory)
#
# @portName     - the device to connect to
# (mandatory)     e.g. /dev/ttyUSB0
#
# @return hash of {canID}{canData}
#
sub readFromBusNoOutput {

  my $debug       = $_[0];
  my $port        = $_[1];

  return readDataFromBus($debug,$port,"NoOutput"); 
}

# Read data from the canbus and store it
# in a hash array
#
# @debug        - 1 to turn debug on
# (mandatory)
#
# @portName     - the device to connect to
# (mandatory)     e.g. /dev/ttyUSB0
#
# @display data - one of:
#                 RollingSummary - give a summary in realtime of canBusID's
#                 NoOutput - don't write anything to STDOUT
#                 FullDetail - write out each message on the canBus
#
# @canIDRegexp  - a regexp used to restrict
# (optional)      what is read from the canbus
#                 to STDOUT
#
# @return hash of {canID}{canData}
#
sub readDataFromBus {

  my $debug       = $_[0];
  my $port        = $_[1];
  my $displayData = $_[2];
  my $canIDRegexp;

  if(@_ == 4) {
    $canIDRegexp = $_[3];
  }
  else {
    $canIDRegexp = "[0-9A-F]{3}";
  }
    

  return readDataFromBusRestrictByRegexp($debug,$port,$displayData,$canIDRegexp);
}

# Read data from the canbus and store it
# in a hash array
#
# @debug    - 1 to turn debug on
#             0 to turn debug off
#
# @portName - the device to connect to
#             e.g. /dev/ttyUSB0
#
# @display data - one of:
#                 RollingSummary - give a summary in realtime of canBusID's
#                 NoOutput - don't write anything to STDOUT
#                 FullDetail - write out each message on the canBus
#
# @canIDRegexp  - a regexp used to restrict
#                 what is read from the canbus
# @return hash
sub readDataFromBusRestrictByRegexp {

  my $debug = $_[0];
  my $port  = $_[1];
  my $displayData = $_[2];
  my $canIDRegexp = $_[3];


  if( !($displayData eq "RollingSummary" ||
      $displayData eq "NoOutput" ||
      $displayData eq "FullDetail") )
  {
    die "Invalid display type of [".$displayData."] in readDataFromBusRestrictByRegexp\n";
  }

  my $displayRolling = $false;
  my $displayNone = $false;
  my $displayFull = $false;

  if( $displayData eq "RollingSummary" )  { $displayRolling = $true; }
    elsif( $displayData eq "NoOutput"  )  { $displayNone = $true; }
    elsif( $displayData eq "FullDetail")  { $displayFull = $true; }

  print "\n";

  runCommandWithPause($debug,$port,"ATMA");

  my $STALL_DEFAULT = 10; # how many seconds to wait for new input
  my $timeout = $STALL_DEFAULT;

  $port->read_char_time(0); # don't wait for each character
  $port->read_const_time(1000); # 1 second per unfulfilled "read" call

  my $inputText = "";
  my $charsRead = 0;
  my $thisPossibleLine;
  my $keyPress;
  my %canbusDataHash;
  my $leftOver = "";
  
  while (1) {
    while ($timeout > 0) {
  
      #look for keypress
      $keyPress = ReadKey(-1);
    
      ($charsRead,$inputText) = $port->read(255);
  
      $inputText = $leftOver.$inputText;
      $charsRead = $charsRead + length($leftOver);
  
      if ( $debug ) { print "\n\nRead input text [".$inputText."]\n"; }
    
      if($charsRead > 0) {
  
        # remove new lines
        $inputText =~ s/\n//g;
  
        if ( $debug ) { print "Input text is [".$inputText."]\n"; }
        
        my @commands = split(/(?=[0-9A-F]{3} )/,$inputText);
  
        # go through all but the last (possibly partial) command
        for(my $thisCommand=0; $thisCommand<$#commands;$thisCommand++) {
  
          if ( $debug ) { print "Found command [".$commands[$thisCommand]."]\n"; }

          #if ( $displayData ) { print $commands[$thisCommand]."\n"; }
  
          my $canbus_id = substr ($commands[$thisCommand], 0, 3);
          my $canbus_payload = 
            substr ($commands[$thisCommand], 3, (length($commands[$thisCommand])-3));

          if( $canbus_id =~ m/^$canIDRegexp/ ) {
            if ( exists $canbusDataHash{$canbus_id}{$canbus_payload} ) {
              $canbusDataHash{$canbus_id}{$canbus_payload} = 
              $canbusDataHash{$canbus_id}{$canbus_payload} + 1; 
	      if ( $displayRolling ) { print "."; }
            }
            else {
              $canbusDataHash{$canbus_id}{$canbus_payload} = 1; 
	      if ( $displayRolling ) { print "[".$canbus_id."]"; }
            }
	    if ( $displayFull ) { print $commands[$thisCommand]."\n"; }
          }
      
        }
  
        $leftOver = $commands[$#commands];
        if ( $debug ) { print "leftover=[".$leftOver."]\n\n"; }
  
      }
      else {
        # no chars
        $timeout --;
      }
    
      if( defined $keyPress ) { last; }
    
    }
  
    if( defined $keyPress ) { last; }
    # had no data for 10 seconds - run another ATMA command
    runCommandWithPause($debug,$port,"");
    runCommandWithPause($debug,$port,"ATMA");
  
    $timeout = $STALL_DEFAULT;

  }

  runCommandWithPause($debug,$port,"");

  print "Done\n";

  return %canbusDataHash;
}
