#!/usr/bin/perl
##############################################################################
#                                                                            #
#  ELcanbus - a tool to aid canbus investigation and reverse engineering.    #
#                                                                            # 
#  Copyright (C) 2014 Simon Allcorn                                          # 
#                                                                            #
#  This file is part of Elcanbus.                                            #
#                                                                            #
#  Elcanbus is free software: you can redistribute it and/or modify          #
#  it under the terms of the GNU General Public License as published by      # 
#  the Free Software Foundation, either version 3 of the License, or         #
#  (at your option) any later version.                                       #
#                                                                            #
#  Elcanbus is distributed in the hope that it will be useful,               #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU General Public License for more details.                              #
#                                                                            #
#  You should have received a copy of the GNU General Public License         #
#  along with ELcanbus.  If not, see <http:#www.gnu.org/licenses/>.          #
#                                                                            #
##############################################################################

use Elcanbus::CanBus 'setupELM327SerialPort', 'runCommandReturnSucess',
  'runCommand', 'runCommandWithPause', 'runCommandExpectOK', 'readFullDetailFromBus',
  'runCommandAndGetResult';
use Term::ReadKey;
use Getopt::Long;
use Time::HiRes qw(gettimeofday tv_interval);
use Log::Log4perl;

use strict;
use warnings;

my $false = 0;
my $true  = 1;

my $debug = $false;

my $installDir = $ENV{'CARSTAIN_HOME'};

# init de-bugger
Log::Log4perl::init($installDir.'/etc/perl_logger.conf');
my $logger = Log::Log4perl->get_logger('CART_LOG');

my $portURL = "/dev/ttyUSB0";
$logger->debug("About to open serial port [".$portURL."]");
my $port = setupELM327SerialPort($debug,$portURL);

my $fifo = $ENV{'CANBUS_CONTROL_FIFO'};
$logger->debug("Using FIFO [".$fifo."]");

$logger->debug("Using CARSTAIN_HOME [".$installDir."]");

#how many bytes to read from the serial port in one go.
my $bytesToRead = 16384;


$logger->debug('About to get ELM327 software version...');

runCommandWithPause($debug,$port,"");
my $result = runCommandAndGetResult($debug,$port,"ATZ\r");
my $elmVersion = "";
if( $result =~ /ELM327[\s]*([\S]*)[\s]*\n/ ) {
 $elmVersion = $1;
}
else {
 $logger->error('Failed to connect to ELM327');
 exit -2;
}

$logger->info("Connect to ELM327. Software version is [".$elmVersion."]");

runCommandReturnSucess($debug,$port,"DUMMY_COMMAND\rATWS\r","ELM327");
runCommandExpectOK($debug,$port,"ATL0\r");
runCommandExpectOK($debug,$port,"ATS0\r");
#runCommandWithPause($debug,$port,"ATMA");

runCommandReturnSucess($debug,$port,"ATMA","");
#runCommand($debug,$port,"ATMA\r\r");

$port->read_char_time(0); # don't wait for each character
$port->read_const_time(5); # 10 milli second per unfulfilled "read" call

my $inputText = "";
my $charsRead = 0;

$logger->info('waiting for core to start reading FIFO...');
open (FIFO_FH, ">".$fifo);

#turn off buffering
{ my $ofh = select FIFO_FH;
  $| = 1;
  select $ofh;
}

# keep an eye on how big the buffer gets
# put some debug out every time it gets bigger
my $maxBufferFill = 0;

while (1) {

  ($charsRead,$inputText) = $port->read($bytesToRead);

  $logger->trace("Chars read = [".$charsRead."]");

  if($charsRead > 0) {

    $logger->trace("number of chars read = [".($charsRead + 0)."]");
    $logger->trace("Read data [".$inputText."]");
    print FIFO_FH $inputText;


    if($charsRead > $maxBufferFill) {
      $maxBufferFill = $charsRead;
      $logger->info("New buffer high tide mark = [".$maxBufferFill."]bytes");
    }
  }
}

$port->close ();

