#!/usr/bin/perl
##############################################################################
#                                                                            #
#  ELcanbus - a tool to aid canbus investigation and reverse engineering.    #
#                                                                            # 
#  Copyright (C) 2014 Simon Allcorn                                          # 
#                                                                            #
#  This file is part of Elcanbus.                                            #
#                                                                            #
#  Elcanbus is free software: you can redistribute it and/or modify          #
#  it under the terms of the GNU General Public License as published by      # 
#  the Free Software Foundation, either version 3 of the License, or         #
#  (at your option) any later version.                                       #
#                                                                            #
#  Elcanbus is distributed in the hope that it will be useful,               #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU General Public License for more details.                              #
#                                                                            #
#  You should have received a copy of the GNU General Public License         #
#  along with ELcanbus.  If not, see <http:#www.gnu.org/licenses/>.          #
#                                                                            #
##############################################################################

use Elcanbus::CanBus 'setupELM327SerialPortWithSpeed',  
  'runCommand', 'runCommandWithPause', 'runCommandExpectOK',  'runCommandReturnSucess',
  'runCommandAndGetResult', 'runCommandExpectPrompt', 'setPPvalue';
use Term::ReadKey;
use Getopt::Long;
use Time::HiRes qw(gettimeofday tv_interval);

use strict;
use warnings;

my $false = 0;
my $true  = 1;

my $debug = $false;

my $portName = "/dev/ttyUSB0";
my $portSpeed;
my $portSpeedFound = $false;
my @portSpeeds = (9600, 19200, 38400, 57600, 115200 );

my $outputText;
my $progressPercent=0;
my $port;

sleep 2;
$progressPercent = $progressPercent +5;
printOutputTextOverwrite("Using port [".$portName."]");

sleep 2;
$progressPercent = $progressPercent +5;
printOutputTextAppend("Getting current baud speed:");

foreach (@portSpeeds) {
 
  $portSpeed = $_;
  $port = setupELM327SerialPortWithSpeed($debug,$portName,$portSpeed);

  runCommandExpectPrompt($debug,$port);
  if(runCommandReturnSucess($debug,$port,"ATZ","ELM327")){
    $portSpeedFound = $true; 
    last;
  }
  else {
    printOutputTextAppend( ".");
  }

  $port->close || printErrorText("failed to close");
  undef $port;

}

if(!$portSpeedFound) {
  printErrorText("*** Could not find port speed ***");
}
else {
  $progressPercent = $progressPercent +5;
  printOutputTextAppend( "found [".$portSpeed."]bps");
}

sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: reset current config");
#turn off everything
runCommandExpectOK($debug,$port,"AT PP FF OFF");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: turn on additional headers");
#turn on additional headers (aka AT H1)
setPPvalue($debug,$port,"01","00");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: set CANBUS long messages");
#allow long messages (aka AT AL)
setPPvalue($debug,$port,"02","00");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: set CANBUS protocol");
#CANBUS protocol 
setPPvalue($debug,$port,"2C","C0");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: set CANBUS speed to 100kbps");
#CANBUS speed
setPPvalue($debug,$port,"2D","05");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: enable CANBUS protocol");
#CANBUS speed
runCommandExpectOK($debug,$port,"AT SP B");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: set serial port speed to 115kbps");
#serial port speed
setPPvalue($debug,$port,"0C","23");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: turn power saving off");
#power saving off
setPPvalue($debug,$port,"0E","1A");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("EEPROM config: reset device");
#Force reset
runCommandReturnSucess($debug,$port,"ATZ","ELM327");
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("Reconnecting at 115kbps...");
#Now reconnect the serial port at 115kbps
$port->close || printErrorText("failed to close");
undef $port;
$port = setupELM327SerialPortWithSpeed($debug,$portName,115200);
sleep 2;

$progressPercent = $progressPercent +5;
runCommandExpectPrompt($debug,$port);
printOutputTextOverwrite("Checking config 1/2...");

$progressPercent = $progressPercent +5;
if(runCommandReturnSucess($debug,$port,"AT PPS",".*01:00\ N.*02:00\ N.*0C:23\ N.*0E:1A\ N.*2C:C0\ N.*2D:05\ N.*>")) {
  printOutputTextOverwrite("Checking config 1/2...ok");
}
else {
  printErrorText("*** config 1/2 of ELM327 has failed ***");
}
sleep 2;

$progressPercent = $progressPercent +5;
printOutputTextOverwrite("Checking config 2/2...");
$progressPercent = 100;
if(runCommandReturnSucess($debug,$port,"AT DP",".*USER1.*CAN.*11/100.*>")) {
  printOutputTextOverwrite("Checking config 2/2...ok");
}
else {
  printErrorText("*** config 2/2 of ELM327 has failed ***");
}
sleep 2;

printOutputTextOverwrite("Configuration of ELM327 completed.");
sleep 5;

exit 0;

sub printOutputTextAppend{

  my $newText = $_[0];
  $outputText = $outputText.$newText;
  printOutputTextOverwrite($outputText);
}

sub printOutputTextOverwrite{

  my $newText = $_[0];

  print "XXX\n";
  print $progressPercent."\n";
  print $newText."\n";
  print "XXX\n";
  $|++;

}

#display a show stopper error message for 10 seconds
#and then exit
sub printErrorText{

  my $newText = $_[0];
  $progressPercent = 100;
  printOutputTextOverwrite($newText);
  sleep 10;
  exit -10;
}

