/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.core;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.allcorn.carstain.utils.Config;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by allcos01 on 08/07/16.
 */
public class CoreTest {

    @Test
    public void checkCanBusRead() throws InterruptedException,ParserConfigurationException,
            SAXException, IOException {

      Logger log = Logger.getLogger(this.getClass());

    /*  Core cartCore=new Core(
              "/Users/allcos01/carstain/carstain/src/test/data/unit-tests/1/raw_canbus.txt",
              Config.getConfig().getCarControlXML());

      cartCore.runCore();
      cartCore.waitForShutdown(1000);
*/

      assertTrue(true);
    }
}
