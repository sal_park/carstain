/*
 * Copyright 2016 Simon Allcorn.
 *
 * This file is part of carstain.
 *
 *     carstain is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     carstain is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with carstain.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.allcorn.carstain.utils;

import static org.junit.Assert.*;

import java.util.regex.Pattern;

import org.junit.Test;

/**
 * @author carstain@allcorn.org
 *
 */
public class CarCommandQueueTest {

  @Test
  public void runTests() {
    
    // Since this is a singleton, we want to run test in a set order
    // because the queue is only created once and used in all tests.
    
    CarCommandQueue myList = new CarCommandQueue(5,3);
    
    this.checkSize(myList);
    this.checkOrder(myList);
    this.checkMaxSize(myList);
    this.readFirstMessageFromList(myList);
    
    this.failedReadOfLongMessage(myList);
    this.failedReadOfCommand(myList);
    
    this.readSameMessageAgain(myList);
   
    this.readSingleCommand(myList);
    
    this.readRestOfQueue(myList);
    
    this.readEmptyQueue(myList);
    this.populateQueueAgain(myList);
    
    this.checkMemoryUsage(myList);
    
  
  }
  
  public void checkSize(CarCommandQueue myList) {
       
    myList.add("111");
    myList.add("222");
    myList.add("333");
    
    assertTrue((myList.size() == 3));
    
    myList.printList("checkSize");
    
  }  
  
  /**
   *  check the commands are in the correct order
   */
  public void checkOrder(CarCommandQueue myList) {
   
    assertTrue((myList.toString().equals("111222333")));
    
    
  }
  
  /**
   *  Check we don't consume lots of memory 
   */
  public void checkMaxSize(CarCommandQueue myList) {

    myList.printList("Before checkMaxSize");
    
    myList.add("444");
    myList.add("555");
    
    myList.printList("Middle checkMaxSize");
    
    assertTrue((myList.size() == 5));
    
    myList.add("666");
    
    assertTrue((myList.size() == 5));
    
    myList.printList("After checkMaxSize");
    
  }
  
  public void checkMemoryUsage(CarCommandQueue myList) {
    
    System.out.println("cap=[" + myList.capacity() + "]");
    System.out.println("Commands in list = [" + myList.size() + "]");
    
    for(int x=0;x<100000;x++) {
      myList.add("666");
    }
    
    System.out.println("Commands in list = [" + myList.size() + "]");
    System.out.println("cap=[" + myList.capacity() + "]");
    assertTrue( myList.capacity() < 35);
    
    
  }
  

  /*
   *   Test a message that is longer than the queue (ie
   *   it has more command elements than the queue currently
   *   contains)
   */
  public void failedReadOfLongMessage(CarCommandQueue myList) {
    
    myList.printList("Before failedReadOfLongMessage");
    
    Pattern pattern = Pattern.compile("^444555666777888999AAA");
    
    assertFalse(myList.match(pattern));
    
    myList.printList("After failedReadOfLongMessage");
    
    
  }

  
  /*
   *   Failed read as the commands we want are in the queue
   *   but not at the start of the queue
   *   
   *   queue contents:
   *   
   *   item[two]
   *   item[three]
   *   item[four]
   *   item[five]
   *   item[six]
   */
  
  public void failedReadOfCommand(CarCommandQueue myList) {
    
    myList.printList("Before failedReadOfCommand");
    
    Pattern pattern = Pattern.compile("555666");
    
    assertFalse(myList.match(pattern));
    
    myList.printList("After failedReadOfCommand");
  }
  
  /*
  /*
   *   the oldest added 2 commands are "222" and "333"
   *   we want to read these off as a message
   *   
   *   queue contents:
   *   (after reading from the queue)
   *   
   *   item[444]
   *   item[555]
   *   item[666]
   */
  public void readFirstMessageFromList(CarCommandQueue myList) {

    myList.add("333");
    myList.add("444");
    myList.add("555");
    myList.add("666");

    myList.printList("before readFirstMessageFromList");
    
    Pattern pattern = Pattern.compile("333444");

    assertTrue(myList.match(pattern));
    
    myList.printList("middle readFirstMessageFromList");
   
    pattern = Pattern.compile("^555666");
    
    assertTrue(myList.match(pattern));
    
    myList.printList("after readFirstMessageFromList");
  
  }
  
  /*
   *   the oldest added 2 commands are now "four" and "five"
   *   if we try to read "two" and "three" again this will fail
   *   as they have been removed from the queue.
   */
  public void readSameMessageAgain(CarCommandQueue myList) {
    
    myList.printList("before readSameMessageAgain");
    
    Pattern pattern = Pattern.compile("^222333");
            
    assertFalse(myList.match(pattern));
    
    myList.printList("after readSameMessageAgain");
   
   
    
    
  }
  
  /* read a single command from carList
   * 
   */
  public void readSingleCommand(CarCommandQueue myList) {

    myList.add("444");
    myList.add("555");

    myList.printList("before readSingleCommand");
    
    Pattern pattern = Pattern.compile("^444");
            
    assertTrue(myList.match(pattern));
    
    myList.printList("after readSingleCommand");
    
  }
  
  
  /* read last 2 elements of queue, queue should now be empty
   * 
   */
  public void readRestOfQueue(CarCommandQueue myList) {
    
    myList.printList("before readRestOfQueue");
    
    Pattern pattern = Pattern.compile("^555");
            
    assertTrue(myList.match(pattern));
    
    myList.printList("after readRestOfQueue");
    
  }
  
  /* try to read from the queue when it is empty
   * 
   */
  public void readEmptyQueue(CarCommandQueue myList) {
    
    myList.printList("before readEmptyQueue");
    
    Pattern pattern = Pattern.compile("^555666777888555666888777");
            
    assertFalse(myList.match(pattern));
    
    myList.printList("after readEmptyQueue");
  }
  
  
  /* populate the queue again and read the commands at the top of the queue
   * 
   */
  public void populateQueueAgain(CarCommandQueue myList) {
       
    myList.add("aaa");
    myList.add("bbb");
    myList.add("ccc");
    myList.add("ddd");
    myList.add("eee");
    myList.printList("before1 populateQueueAgain");
    
    myList.add("fff");
    myList.add("ggg");
    myList.printList("before2 populateQueueAgain");
    
    myList.add("hhh");
    myList.add("iii");
    myList.printList("before3 populateQueueAgain");
    
    myList.add("jjj");
    myList.add("kkk");
    myList.printList("before4 populateQueueAgain");
    
    myList.add("lll");
    myList.add("mmm");
    
    // Dont forget, queue length is only 5, so contents
    // will be i,j,k,l and m
    
    
    Pattern pattern = Pattern.compile("^iiijjjkkk");
    
    myList.printList("before5 populateQueueAgain");
    
    assertTrue(myList.match(pattern));
    
    myList.printList("after populateQueueAgain");   
  }
  
}
